FROM openjdk:12
ADD target/docker-spring-boot-lucianosobucki.jar docker-spring-boot-lucianosobucki.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-lucianosobucki.jar"]

